﻿namespace SharedModel
{
    /// <summary>
    /// TelephoneBook.
    /// </summary>
    public class TelephoneBook
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public int TelephoneBookId { get; set; }

		/// <summary>
		/// Имя.
		/// </summary>
		public string FirstName { get; set; }

		/// <summary>
		/// Фамилия.
		/// </summary>
		public string LastName { get; set; }

		/// <summary>
		/// NumberPhone.
		/// </summary>
		public string NumberPhone { get; set; }

		public List<Task> Tasks { get; } = new();
	}
}
