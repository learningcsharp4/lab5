﻿using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using Server.Database;
using SharedModel;

namespace Server
{
	public class TelephoneBookController : WebApiController
	{
		#region Константы

		/// <summary>
		/// Логгер.
		/// </summary>
		private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

		#endregion

		[Route(HttpVerbs.Get, "/tbbyid/{id}")]
		public TelephoneBook? GetTelephoneBookById(int id)
		{
			Logger.Info($"Get TelephoneBook by {id}.");
			using (var db = new TaskTrackerContext())
			{
				return db.TelephoneBooks.FirstOrDefault(p => p.TelephoneBookId == id);
			}
		}

		[Route(HttpVerbs.Get, "/alltb")]
		public List<TelephoneBook> GetAllTelephoneBook()
		{
			Logger.Info("Get all TelephoneBook.");
			using (var db = new TaskTrackerContext())
			{
				return db.TelephoneBooks.ToList();
			}
		}

		[Route(HttpVerbs.Post, "/addtb")]
		public TelephoneBook? SaveTelephoneBook([JsonData] TelephoneBook telephoneBook)
		{
			Logger.Info("Save TelephoneBook.");
			using (var db = new TaskTrackerContext())
			{
				var result = db.Add(telephoneBook);
				db.SaveChanges();
				return result?.Entity;
			}
		}

		[Route(HttpVerbs.Put, "/updatb")]
		public TelephoneBook? UpdateTelephoneBook([JsonData] TelephoneBook telephoneBook)
		{
			Logger.Info("Update TelephoneBook.");
			using (var db = new TaskTrackerContext())
			{
				var result = db.TelephoneBooks.FirstOrDefault(p => p.TelephoneBookId == telephoneBook.TelephoneBookId);
				if (result == null)
				{
					Logger.Info("TelephoneBook not found.");
					return null;
				}

				result.FirstName = telephoneBook.FirstName;
				result.LastName = telephoneBook.LastName;
				result.NumberPhone = telephoneBook.NumberPhone;

				db.SaveChanges();
				return result;
			}
		}

		[Route(HttpVerbs.Post, "/deletetbbyid/{id}")]
		public bool DeleteTelephoneBook(int id)
		{
			Logger.Info("Delete TelephoneBook.");
			using (var db = new TaskTrackerContext())
			{
				var result = db.TelephoneBooks.FirstOrDefault(p => p.TelephoneBookId == id);
				if (result == null)
				{
					Logger.Info("TelephoneBook not found.");
					return false;
				}

				db.Remove(result);
				db.SaveChanges();
				return true;
			}
		}
	}
}
