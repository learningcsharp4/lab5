﻿using SharedModel;
using Task = SharedModel.Task;

namespace Client
{
	public class Program
	{
        #region Константы
        /// <summary>
        /// Логгер.
        /// </summary>
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
		#endregion


        static void Main(string[] args)
		{
            Logger.Trace("Start program.");
            Console.WriteLine("Start!");

			var telephoneBook = new TelephoneBook()
			{
                TelephoneBookId = 31,
				FirstName = "ИмяЧеловека",
				LastName = "ФамилияЧеловека",
				NumberPhone = "123456789122"
			};

			var task = new Task()
			{
				TaskId = 33,
				Title = "Дело из списка человека",
				Description = "Описание дела",
				Deadline = DateTime.Now,
                TelephoneBook = telephoneBook,
                TelephoneBookId = 31
			};

			try
			{
				var apiTask = new ApiClient<Task>();
				var apiPeople = new ApiClient<TelephoneBook>();

				Console.WriteLine("Create task...");
				Logger.Trace("Create task.");
				Console.WriteLine(apiTask.CreateAsync(task).GetAwaiter().GetResult());

                Logger.Trace("Geting TelephoneBook.");
                Console.WriteLine("Get TelephoneBook...");
				var result = apiPeople.GetAsync("1").GetAwaiter().GetResult();
				Console.WriteLine($"{result?.FirstName} {result?.LastName}");
			}
			catch (Exception ex)
			{
                Logger.Trace(ex);
                Console.WriteLine(ex.Message);
			}
            Logger.Trace("End program.");
        }
	}
}